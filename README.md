# Welcome!
![rainbow dimer](dimer.jpg)

This is a project of our scratchwork working on the node smooshing algorithm in the dimer model. We want to have code that generates the graphs and adjacency/Kasteleyn/Gessel-Viennot matrices, and that goes back and forth between those categories. We have an eventual goal of clicking on the edge that we want to collapse which will then do the relevant matrix operations and display the updated graph.
