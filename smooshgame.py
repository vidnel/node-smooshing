#!/usr/bin/python3

import numpy as np
import pygame
import sys


###############################################################
###################       graph setup      ####################
###############################################################

"""
creates the positioned list of vertices for an a x b x c hexagon graph
"""
def make_vertices(a, b, c):

    factor = np.sqrt(3)/2
    # makes the height and width of the whole grid too big to start
    height = 2*c + a + b
    width = int(1.5*(a + b)+1)

    #creates the whole grid of hex points (square grid for now)
    vertex_list = []
    center_list = []
    for i in range(-width, width):
        for j in range(-height, height):
            # shifts every other row left by 1.5
            if j % 2 == 1:
                if i % 3 != 2:
                    vertex_list.append((i - 3/2, j * factor))
                else:
                    center_list.append((i - 3/2, j * factor))
            # non-shifted rows
            elif i % 3 != 2:
                vertex_list.append((i, j * factor))
            elif i % 3 == 2:
                center_list.append((i, j * factor))

    #trims the list of vertices based on the following six inequalities
    trimmed_list = []
    trimmed_center_list = []
    for vertex in vertex_list:
        if (vertex[1] <= factor * (1/1.5*vertex[0] + 2*c) and # this one controls the height on the left
            vertex[1] <= factor * (-1/1.5*vertex[0] + 2*c + 2*(a - 1) + 1) and
            vertex[0] >= -0.5 and 
            vertex[0] <= (a + b - 1)*1.5 and 
            vertex[1] >= factor * (-1/1.5*vertex[0]) and 
            vertex[1] >= factor * (1/1.5*vertex[0] + 1/3 - (2*b-1))
           ):
            trimmed_list.append(vertex)

    for vertex in center_list:
        if (vertex[1] <= factor * (1/1.5*vertex[0] + 2*c) and # this one controls the height on the left
            vertex[1] <= factor * (-1/1.5*vertex[0] + 2*c + 2*(a - 1) + 1) and
            vertex[0] >= -0.5 and 
            vertex[0] <= (a + b - 1)*1.5 and 
            vertex[1] >= factor * (-1/1.5*vertex[0]) and 
            vertex[1] >= factor * (1/1.5*vertex[0] + 1/3 - (2*b-1))
           ):
            trimmed_center_list.append(vertex)

    # creates a list of all included x- and y-values
    x_vals = []
    y_vals = []
    for vertex in trimmed_list:
        x_vals.append(vertex[0])
        y_vals.append(vertex[1])

    # shifts the set of vertices down and right so the whole graph is drawn on screen
    shifted_list = []
    shifted_center_list = []
    for vertex in trimmed_list:
        shifted_list.append((vertex[0] - min(x_vals) + 0.5, vertex[1] - min(y_vals) + 0.5))
    for vertex in trimmed_center_list:
        shifted_center_list.append((vertex[0] - min(x_vals) + 0.5, vertex[1] - min(y_vals) + 0.5))
    return shifted_list, shifted_center_list


"""
uses the scaling factor to scale the drawn vertices
"""
def scale_vertices(vertex_list, scale_factor):
    return_vertices = []
    for i in vertex_list:
        return_vertices.append(tuple([(i[0])*scale_factor, (i[1])*scale_factor]))

    return return_vertices


"""
takes in the list of edges and a dictionary with keys as vertices, and edges as entries. 
also sets up a matching dictionary with keys as edges and a single entry as the matched edge connected to that vertex
"""
def vertex_edge_dict(edge_list, matched_hexes):
    return_dict = {}
    for edge in edge_list:
        vert1 = edge[0]
        vert2 = edge[1]
        if vert1 not in return_dict:
            return_dict[vert1] = [edge]
        else:
            return_dict[vert1].append(edge)
        if vert2 not in return_dict:
            return_dict[vert2] = [edge]
        else:
            return_dict[vert2].append(edge)

    match_dict = {}
    for hexagon in matched_hexes:
        for edge in matched_hexes[hexagon]:
            vert1 = edge[0]
            vert2 = edge[1]
            if vert1 not in match_dict:
                match_dict[vert1] = [edge]
            elif edge not in match_dict[vert1]:
                match_dict[vert1].append(edge)
            if vert2 not in match_dict:
                match_dict[vert2] = [edge]
            elif edge not in match_dict[vert2]:
                match_dict[vert2].append(edge)

    return return_dict, match_dict


""" 
takes in list of UNSCALED vertices and returns a list of edges in the form edge = [(x1,y1), (x2,y2)]
also takes in a scaling factor so that when the edges are created you can stretch them on the screen
"""
def make_box(edge):

    if max(edge[0][1], edge[1][1]) - min(edge[0][1], edge[1][1]) <= 1:
        y_val = 10
        y_min = min(edge[0][1], edge[1][1]) - 5
    else: 
        y_val = max(edge[0][1], edge[1][1]) - min(edge[0][1], edge[1][1])
        y_min = min(edge[0][1], edge[1][1])

    new_box = pygame.Rect(min(edge[0][0], edge[1][0]),
                            y_min,
                            max(edge[0][0], edge[1][0]) - min(edge[0][0], edge[1][0]),
                            y_val)
    return new_box

""" 
creates the edges given a list of vertices by checking if they are a distance of one apart, 
then scales based on the global scaling factor
"""

def make_edges(my_vertices, scale_factor):
    my_edges = []
    for i in range(len(my_vertices)):
        for j in range(i, len(my_vertices)):
            distance = (my_vertices[i][0] - my_vertices[j][0])**2 + (my_vertices[i][1] - my_vertices[j][1])**2
            if distance <= 1.01 and distance >= 0.99:
                my_edges.append([(my_vertices[i][0]*scale_factor, my_vertices[i][1]*scale_factor), (my_vertices[j][0]*scale_factor, my_vertices[j][1]*scale_factor)])


    # creates invisible rectangles around each edge
    my_boxes = []
    for edge in my_edges:
        my_boxes.append(make_box(edge))
        # checks to see if it's a horizontal edge, and if so it makes the click-box
        # 10 pixels taller for ease of clicking

    return my_edges, my_boxes

"""
returns a dictionary hexagons where the key is hexagon_center, value = [list of all edges around that center]
"""
def get_hexagons(center_list, edge_list):
    hexagon_dict = {} #key is the center of the hex, values are the list of edges
    for edge in edge_list:
        for center in center_list:
            if ((edge[0][0] - center[0])**2 + (edge[0][1] - center[1])**2 <= (1.05 * scale_factor * scale_factor)) and ((edge[1][0] - center[0])**2 +( edge[1][1] - center[1])**2 <= (1.05 * scale_factor * scale_factor)):
                if center not in hexagon_dict:
                    hexagon_dict[center] = [edge]
                else:
                    hexagon_dict[center].append(edge)
    return hexagon_dict

"""
returns a dictionary of hexagons where they key = hexagon center, value = [edges in matching around that center]
always has between 1 and 3 edges fort he hexagons
"""
def get_matching_hexagons(center_list, matching_list): 
    hexagon_dict = {} #key is the center of the hex, values are the list of edges
    for edge in matching_list:
        for center in center_list:
            if ((edge[0][0] - center[0])**2 + (edge[0][1] - center[1])**2 <= (1.05 * scale_factor * scale_factor)) and ((edge[1][0] - center[0])**2 +( edge[1][1] - center[1])**2 <= (1.05 * scale_factor * scale_factor)):
                if center not in hexagon_dict:
                    hexagon_dict[center] = [edge]
                else:
                    hexagon_dict[center].append(edge)
    return hexagon_dict

"""
returns a list of centers of all hexagons that can toggle (have exactly three matched edges)
"""
def can_toggle(matched_hexagons):
    togglable = []
    for gon in matched_hexagons:
        if len(matched_hexagons[gon]) == 3:
            togglable.append(gon)
    return togglable

"""
returns a list of all hexagons that are adjacent to a given center: returns between 3 and 6 centers
"""
def adjacent_hexes(center, center_list):
    adjacent = []
    for hexagon in center_list:
        if ((hexagon[0] - center[0])**2 + (hexagon[1] - center[1])**2 <= (4 * scale_factor * scale_factor)):
            if hexagon != center:
                adjacent.append(hexagon)
    return adjacent

"""
creates a list of all edge vertices with two edges
(so all the ones that are valid node choices on the edges of the graph)
"""
def make_node_list(edge_dict):
    nodes = []
    for vertex in edge_dict:
        if len(vert_key_edge_dict[vertex]) == 2:
            nodes.append(vertex)
    return nodes



###############################################################
#############      creating matchings      ####################
###############################################################

"""
finds the minimum matching of an axbxc hexagon graph by checking the slopes
of each edge on any side of the 'center' lines
"""
def minimum_matching(edge_list, a, b, c):
    left_trident = []
    right_trident = []
    bottom_trident = []

    x_min = 999999999999
    y_min = 999999999999
    x_max = -999999999999
    y_max = -999999999999
    corresp_x_min = -999999999999 # is a y-value
    corresp_x_max = -999999999999
    corresp_y_min = 999999999999
    for edge in edge_list:
        for vert in edge:
            if vert[0] < x_min: #minx, largest y
                x_min = vert[0] # max y-val corresponding to min x
            if vert[0] >= x_max:
                x_max = vert[0]
#                if vert[1] > x_max_y:
#                    x_max_y = vert[1]
            if vert[1] < y_min:
                y_min = vert[1]
                corresp_y_min = vert[0]
            if vert[1] > y_max:
                y_max = vert[1]

    for edge in edge_list:
        for vert in edge:
            if vert[0] == x_min:
                if corresp_x_min < vert[1]:
                    corresp_x_min = vert[1]

            if vert[0] == x_max:
                if corresp_x_max < vert[1]:
                    corresp_x_max = vert[1]

    if [(corresp_y_min, y_min), (corresp_y_min + scale_factor, y_min)] not in edge_list and [(corresp_y_min + scale_factor, y_min), (corresp_y_min, y_min)] not in edge_list:
        corresp_y_min -= scale_factor

    # | line is at x-val corresponding to minimum y-val
    # max y corresponding to min x is / line
    # max y corresponding to max x is \ line
    for edge in edge_list:
        if (edge[0][0] < corresp_y_min + 0.5*scale_factor and 
            edge[1][0] < corresp_y_min + 0.5*scale_factor and 
            edge[0][1] < -np.sqrt(3)/3 * (edge[0][0] - (2 * x_min + 0.5*scale_factor)/2) + (2 * corresp_x_min + (np.sqrt(3)/2 * scale_factor))/2 and 
            edge[1][1] < -np.sqrt(3)/3 * (edge[1][0] - (2 * x_min + 0.5*scale_factor)/2) + (2 * corresp_x_min + (np.sqrt(3)/2 * scale_factor))/2
            ):
            left_trident.append(edge)
        if (edge[0][0] > corresp_y_min + 0.5*scale_factor and 
            edge[1][0] > corresp_y_min + 0.5*scale_factor and 
            edge[0][1] < np.sqrt(3)/3 * (edge[0][0] - (2 * x_max - 0.5*scale_factor)/2) + (2 * corresp_x_max + np.sqrt(3)/2 * scale_factor)/2 and 
            edge[1][1] < np.sqrt(3)/3 * (edge[1][0] - (2 * x_max - 0.5*scale_factor)/2) + (2 * corresp_x_max + np.sqrt(3)/2 * scale_factor)/2
            ):
            right_trident.append(edge)
        if (edge[0][1] > np.sqrt(3)/3 * (edge[0][0] - (2 * x_max - 0.5*scale_factor)/2) + (2 * corresp_x_max + np.sqrt(3)/2 * scale_factor)/2 and 
            edge[1][1] > np.sqrt(3)/3 * (edge[1][0] - (2 * x_max - 0.5*scale_factor)/2) + (2 * corresp_x_max + np.sqrt(3)/2 * scale_factor)/2 and 
            edge[0][1] > -np.sqrt(3)/3 * (edge[0][0] - (2 * x_min + 0.5*scale_factor)/2) + (2 * corresp_x_min + (np.sqrt(3)/2 * scale_factor))/2 and 
            edge[1][1] > -np.sqrt(3)/3 * (edge[1][0] - (2 * x_min + 0.5*scale_factor)/2) + (2 * corresp_x_min + (np.sqrt(3)/2 * scale_factor))/2
            ):
            bottom_trident.append(edge)


    minimal_matching = []
    for edge in left_trident:
        tup_1 = edge[0]
        tup_2 = edge[1]
        if (tup_2[0] - tup_1[0])/(tup_2[1] - tup_1[1]) < 0.01:
            minimal_matching.append(edge)


    for edge in right_trident:
        tup_1 = edge[0]
        tup_2 = edge[1]
        slope = (tup_1[1] - tup_2[1])/(tup_1[0] - tup_2[0])
        if slope > 0.01:
            minimal_matching.append(edge)


    for edge in bottom_trident:
        tup_1 = edge[0]
        tup_2 = edge[1]
        slope = (tup_2[1] - tup_1[1])/(tup_2[0] - tup_1[0])
        if slope <= 0.01 and slope >= -0.01:
            minimal_matching.append(edge)

    return minimal_matching

"""
switches the matched edges in a togglable hexagon, and updates the surrounding hexagons, too
"""
def toggle_hexagon(center, matched_hexagons, hexagon_list):
    adjacent_list = adjacent_hexes(center, matched_hexagons.keys())
    original_edges = hexagon_list[center]
    original_matched_edges = matched_hexagons[center]
    new_center_matching = original_matched_edges.copy()
    for edge in original_edges: 
        for hexagon in adjacent_list:
            current_hex_edges = hexagon_list[hexagon]
            new_hex_edges = current_hex_edges.copy()
            current_hex_matching = 0
            current_hex_matching = matched_hexagons[hexagon]
            new_hex_matching = current_hex_matching.copy()
            if edge in original_matched_edges:
                if edge in new_center_matching:
                    new_center_matching.remove(edge)
                if edge in new_hex_matching:
                    new_hex_matching.remove(edge)
            else:
                if edge not in new_center_matching:
                    new_center_matching.append(edge)
                if edge not in new_hex_matching and edge in hexagon_list[hexagon]:
                    new_hex_matching.append(edge)

            matched_hexagons[hexagon] = new_hex_matching

    matched_hexagons[center] = new_center_matching

    return matched_hexagons

"""
takes in a current matching and then toggles a random choice of togglable hexagons 'number_of_toggles' times
"""
def random_matching(number_of_toggles, matched_hexagons, hexagon_list):

    current_matching = matched_hexagons.copy()

    for i in (range(number_of_toggles)):
        current_togglable_list = can_toggle(current_matching)
        new_center_index = np.random.choice(range(len(current_togglable_list)))
        new_center = current_togglable_list[new_center_index]
        current_matching = toggle_hexagon(new_center, current_matching, hexagon_list)

    return current_matching


"""
adds or removes a single matched edge in all hexagons containing that edge
"""
def update_hexagon(edge, hexagons, matched_hexagons):
    matched_hexagons1 = matched_hexagons.copy()
    for center in hexagons:
        if edge in hexagons[center]:
            if edge in matched_hexagons[center]:
                matched_hexagons1[center].remove(edge)
            else:
                matched_hexagons1[center].append(edge)
    return matched_hexagons1

"""
the distance formula
"""
def distance(vert1, vert2):
    return (vert1[0] - vert2[0])**2 + (vert1[1] - vert2[1])**2

"""
uses a loop-erased random walk through the graph to create a path from one node to another
then toggles the matching of every edge in that path to create nodes that are no longer attached to matched edges
"""
def path_flip(start_vert, end_vert, edge_dict, match_dict, hexagons, matched_hexagons):

    path_edges = []
    visited_nodes = [start_vert]

    # the first step should be along a matched edge
    selected_edge = match_dict[start_vert]
    print(selected_edge)

    # edges could either be a -> b or b -> a, so check:
    if selected_edge[0][0] == start_vert:
        new1_vert = selected_edge[0][1]
    if selected_edge[0][1] == start_vert:
        new1_vert = selected_edge[0][0]

    # take the step
    current_position = new1_vert
    visited_nodes.append(current_position)
    path_edges.append(selected_edge[0])

    at_end = False

    # take a random walk with at most 1000 steps 
    # (if you get to 1000 it's a failure)
    while at_end == False and len(visited_nodes) <= 1000:

        ##### ADD EDGE STEP #####
        # get list of possible connected edges
        edge_candidate_list = edge_dict[current_position].copy()

        if selected_edge[0] in edge_candidate_list:
            edge_candidate_list.remove(selected_edge[0])

        edge_index = np.random.choice(len(edge_candidate_list))
        edge_candidate = edge_candidate_list[edge_index]

        if edge_candidate[0] == current_position:
            new_position = edge_candidate[1]
        if edge_candidate[1] == current_position:
            new_position = edge_candidate[0]

        current_position = new_position
        if current_position in visited_nodes:
            #print(current_position, 'is a duplicate')
            bad_index = visited_nodes.index(current_position)
            #print(visited_nodes)
            #print(visited_nodes[bad_index-2], visited_nodes[bad_index-1], visited_nodes[bad_index], visited_nodes[bad_index+1], visited_nodes[bad_index+2])
            if path_edges[bad_index] in match_dict[current_position]:
                visited_nodes = visited_nodes[:(bad_index+1)]
                path_edges = path_edges[:(bad_index)]
            else:
                visited_nodes = visited_nodes[:(bad_index-0)]
                path_edges = path_edges[:(bad_index-1)]
        else:
            path_edges.append(edge_candidate)
        visited_nodes.append(current_position)

        if current_position == end_vert:
            at_end = True


        ####### REMOVE EDGE STEP #####

        selected_edge = match_dict[current_position]

        if selected_edge in path_edges:
            visited_nodes = visited_nodes[:(bad_index-0)]
            path_edges = path_edges[:(bad_index-1)]
            #continue


        if selected_edge[0][0] == current_position:
            new1_vert = selected_edge[0][1]
        if selected_edge[0][1] == current_position:
            new1_vert = selected_edge[0][0]

        current_position = new1_vert
        visited_nodes.append(current_position)
        path_edges.append(selected_edge[0])

        if current_position == end_vert:
            at_end = True

    print('done after {} steps'.format(len(visited_nodes)))

    matched_hexes = matched_hexagons
    for edge in path_edges:
        matched_hexes = update_hexagon(edge, hexagons, matched_hexes)
    return matched_hexes, path_edges, visited_nodes

"""
allows us to have a set of two non-crossing paths
"""
def flip_two_paths(mstart, 
                   mend, 
                   nstart, 
                   nend, 
                   edge_dict, 
                   match_dict, 
                   hexagons, 
                   matched_hexagons):

    m_hexes, m_edges, m_nodes = path_flip(mstart, mend, edge_dict, match_dict, hexagons, matched_hexagons)
    n_hexes, n_edges, n_nodes = path_flip(nstart, nend, edge_dict, match_dict, m_hexes, matched_hexagons)

    matches = set(m_nodes) & set(n_nodes)

    j = 0
    while len(matches) > 0:
        m_hexes, m_edges, m_nodes = path_flip(mstart, mend, edge_dict, match_dict, hexagons, matched_hexagons)
        n_hexes, n_edges, n_nodes = path_flip(nstart, nend, edge_dict, match_dict, hexagons, m_hexes)

        matches = set(m_nodes) & set(n_nodes)

        j += 1
        if j > 20:
            print("These nodes have a forced intersection")
            break

    return n_hexes, n_edges, m_edges



###############################################################
##############      smooshing things      #####################
###############################################################

"""
takes in a list of edges and a given index, then smooshes the edge at that index
"""
def edge_smoosh(edge_list, edge_box_list, edge_index, vertices):

    old_edge = edge_list[edge_index]
    old_box = edge_box_list[edge_index]
    v1 = old_edge[0]
    v2 = old_edge[1]

    #the new position is the average of the positions of v1 and v2 given as a tuple
    new_position = ((old_edge[0][0] + old_edge[1][0])/2,
                    (old_edge[0][1] + old_edge[1][1])/2)

    brand_new_vertices = [new_position]
    for vertex in vertices:
        if vertex != v1 and vertex != v2:
            brand_new_vertices.append(vertex)

    edges_to_delete = []
    new_edges = []
    boxes_to_delete = []
    new_boxes = []

    for i, edge in enumerate(edge_list):
        if edge == old_edge:
            pass
        elif edge[0] == v1:
            new_edges.append((new_position, edge[1]))
            edges_to_delete.append(edge)
            boxes_to_delete.append(edge_box_list[i])
            new_boxes.append(make_box(edge))
        elif edge[0] == v2:
            new_edges.append((new_position, edge[1]))
            edges_to_delete.append(edge)
            boxes_to_delete.append(edge_box_list[i])
            new_boxes.append(make_box(edge))
        elif edge[1] == v1:
            new_edges.append((new_position, edge[0]))
            edges_to_delete.append(edge)
            boxes_to_delete.append(edge_box_list[i])
            new_boxes.append(make_box(edge))
        elif edge[1] == v2:
            new_edges.append((new_position, edge[0]))
            edges_to_delete.append(edge)
            boxes_to_delete.append(edge_box_list[i])
            new_boxes.append(make_box(edge))

    brand_new_edges = []
    brand_new_boxes = []

    for edge in edge_list:
        if edge not in edges_to_delete and edge != old_edge:
            brand_new_edges.append(edge)

    for edge in new_edges:
        brand_new_edges.append(edge)

    for box in edge_box_list:
        if box not in boxes_to_delete and box != old_box:
            brand_new_boxes.append(box)

    for box in new_boxes:
        brand_new_boxes.append(box)

    return brand_new_vertices, brand_new_edges, brand_new_boxes

"""
collapses a random subset of edges of a perfect matching
"""
def collapse_matched(matching, edge_list, edge_box_list, n, vertices): # n is number to collapse

    new_verts = vertices.copy()
    new_edges = edge_list.copy()
    new_boxes = edge_box_list.copy()
    smoosh_edges = []
    while len(smoosh_edges) < n:
        edge_candidate = np.random.choice(range(len(new_edges)))
        if new_edges[edge_candidate] in matching:
            smoosh_edges.append(edge_candidate)

            new_verts, new_edges, new_boxes = edge_smoosh(new_edges, new_boxes, edge_candidate, new_verts)


    return new_verts, new_edges, new_boxes

###############################################################
##############      rendering things      #####################
###############################################################

"""
globally sets a scaling factor, and makes the actual vertices 
and edges to be drawn later
"""
scale_factor = 60
a = 6
b = 5
c = 4
unscaled_vertex_list, unscaled_center_list = make_vertices(a, b, c) # \ by / by |
edge_list, edge_rect_list = make_edges(unscaled_vertex_list, scale_factor)
vertex_list = scale_vertices(unscaled_vertex_list, scale_factor)
center_list = scale_vertices(unscaled_center_list, scale_factor)
hexagon_list = get_hexagons(center_list, edge_list)

min_matching = minimum_matching(edge_list, a, b, c)
matched_hexagons = get_matching_hexagons(center_list, min_matching)

vert_key_edge_dict, vert_key_matching_dict = vertex_edge_dict(edge_list, matched_hexagons)
node_list = make_node_list(vert_key_edge_dict)


"""
calls the draw circle function in pygame to draw a dots at the location of each vertex in the requested color
"""
def render_vertex(coord_tuple, color, thickness = 4):
    point = (coord_tuple[0], coord_tuple[1])
    pygame.draw.circle(screen, color, point, thickness)

"""
calls the draw line function in pygame to draw a line from coord1 to coord2 in the requested color
"""
def render_edge(coord1, coord2, color, thickness = 4):
    pygame.draw.line(screen, color, coord1, coord2, thickness)

"""
takes in a color position value, and a number of breakdowns for the rainbow and returns a color
"""
def rainbow(i, number_of_colors):
    step_value = int(1530/number_of_colors)
    my_color = 0,0,0
    color_number = int(i * step_value)

    # red to yellow
    if color_number <= 255:
        my_color = 255, color_number, 0

    # yellow to green
    if color_number > 255 and color_number <= 510:
        my_color = 255 - (color_number % 255), 255, 0

    # green to cyan
    if color_number > 510 and color_number <= 765:
        my_color = 0, 255, color_number - 510

    # cyan to blue 
    if color_number > 765 and color_number <= 4*255:
        my_color = 0, 255 - (color_number % 255), 255

    # blue to magenta
    if color_number > 4 * 255 and color_number <= 5 * 255:
        my_color = color_number - (4*255), 0, 255

    # magenta to red
    if color_number > 5 * 255 and color_number <= 6 * 255:
        my_color = 255, 0, 255 - (color_number % 255)

    return my_color

"""
renders the vertices and edges for the first time
"""
def render_everything(vertex_list, edge_list):
    screen.fill(white)
    j = 0
    for i in edge_list:
        tup_1 = i[0]
        tup_2 = i[1]
        render_edge(tup_1, tup_2, rainbow(j, len(edge_list)))
        j += 1
        # for a random color, replace the color above with colors_list[np.random.choice(len(colors_list))]
        # for a rainbow over all the edges, replace the color with 
        "rainbow(j, len(edge_list))"

    # goes through the list of vertices to draw the dots on top of the edges    
    for i in vertex_list:
        render_vertex(i, black)


##############################################    
######           PYGAME STUFF          #######
##############################################

pygame.init()
size = width, height = 1020, 900

# generate some custom colors since full defaults are garish
black = 0, 0, 0 
white = 255, 255, 255
blue = 100, 100, 255
green = 0, 150, 0
red = 255, 50, 50
purple = 150, 50, 220
teal = 100, 200, 200
magenta = 255, 100, 255
yellow = 255, 255, 50
colors_list = [blue, green, red, purple, teal, magenta, yellow]


#renders the screen
screen = pygame.display.set_mode(size)

#sets the background to be white
screen.fill(white)

# goes through the edge list to draw the edges
for j, i in enumerate(edge_list):
    tup_1 = i[0]
    tup_2 = i[1]
    #render_edge(tup_1, tup_2, black, 6)
    render_edge(tup_1, tup_2, rainbow(j, len(edge_list)))
    # for a random color, replace the color above with colors_list[np.random.choice(len(colors_list))]
    # for a rainbow over all the edges, replace the color with 
    "rainbow(j, len(edge_list))"

# goes through the list of vertices to draw the dots on top of the edges    
for j,i in enumerate(vertex_list):
    render_vertex(i, black, 6)

for i in center_list:
    render_vertex(i, teal, 8)

######################
new_thing = toggle_hexagon(can_toggle(matched_hexagons)[0], matched_hexagons, hexagon_list)

new_matching_hexes = random_matching(4000, matched_hexagons, hexagon_list)

new_matching_list = []

for center in new_matching_hexes:
    for thing in new_matching_hexes[center]:
        new_matching_list.append(thing)


new_matching_list1 = []

node1_index = np.random.choice(len(node_list))
node1 = node_list[node1_index]
node2_index = np.random.choice(len(node_list))
node2 = node_list[node2_index]
node3_index = np.random.choice(len(node_list))
node3 = node_list[node3_index]
node4_index = np.random.choice(len(node_list))
node4 = node_list[node4_index]

vert_key_edge_dict, vert_key_matching_dict = vertex_edge_dict(edge_list, new_matching_hexes)
node_list = make_node_list(vert_key_edge_dict)

#new_matching_hexes1, path_edges = path_flip(start_vert = node1, end_vert = node2, edge_dict = vert_key_edge_dict, match_dict = vert_key_matching_dict, hexagons = hexagon_list, matched_hexagons = new_matching_hexes)

new_matching_hexes1, path_edges1, path_edges2 = flip_two_paths(mstart = node1, mend = node2, nstart = node3, nend = node4, edge_dict = vert_key_edge_dict, match_dict = vert_key_matching_dict, hexagons = hexagon_list, matched_hexagons = new_matching_hexes)



for i, edge in enumerate(path_edges1):
    vert1 = edge[0]
    vert2 = edge[1]
    render_edge(vert1, vert2, rainbow(i, len(path_edges1)), 40)

for i, edge in enumerate(path_edges2):
    vert1 = edge[0]
    vert2 = edge[1]
    render_edge(vert1, vert2, rainbow(i, len(path_edges2)), 40)

for i in new_matching_list:
    render_edge(i[0], i[1], color = purple, thickness = 18)

render_vertex(node1, red, 30)
render_vertex(node2, blue, 30)
render_vertex(node3, red, 40)
render_vertex(node4, blue, 40)


for center in new_matching_hexes1:
    for edge in new_matching_hexes1[center]:
        new_matching_list1.append(edge)


#for i in new_matching_list1:
#    render_edge(i[0], i[1], color = magenta, thickness = 12)



# collapses a random matching
#new_verts, new_edges, new_boxes = collapse_matched(matching1, edge_list, edge_rect_list, 26, vertex_list)
#render_everything(vertex_list = vertex_list, edge_list = new_matching_list)


# needs to be a continual loop to keep drawing the screen? I think
while True:
    for event in pygame.event.get():
        if event.type == pygame.MOUSEBUTTONUP:
            mouse_location = pygame.mouse.get_pos()
            mouse_rect = pygame.Rect(mouse_location[0], mouse_location[1], 1, 1)
            clicked_edge = mouse_rect.collidelist(edge_rect_list)
            if clicked_edge == -1:
                break

            new_vertices, new_edges, new_boxes = edge_smoosh(edge_list, edge_rect_list, clicked_edge, vertex_list)

            render_everything(vertex_list = new_vertices, edge_list = new_edges)

            edge_rect_list = new_boxes.copy()
            edge_list = new_edges.copy()
            vertex_list = new_vertices.copy()


        if event.type == pygame.QUIT: sys.exit()
    pygame.display.flip()
